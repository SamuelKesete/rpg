using RPG_Characters.Characters;
using RPG_Characters.Eceptions;
using RPG_Characters.Items;

namespace InventoryManagerTest
{
    public class CharactersUnitTest
    {
        [Fact]
        public void MageConstructor_InitiliazCharacter_SouldBeLevelOne()
        {
            // Arrange
            Mage mage = new ();
            int expected = 1;
            // Act
            int actual = mage.Level;
            //Assert 
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_CharacterLevelUP_ShouldBeLevelTwo()
        {
            // arrange
            int expected = 2;
            Mage mage = new Mage();
            // act
            mage.LevelUP();
            int actual = mage.Level;
            //assert
            Assert.Equal (expected, actual);    
            

        }

        [Fact]
        public void WarriorAttributes_NewWarriorCharacterAttributes_ShouldBeLevelOneAttributes()
        {
            // arrange
            Warrior warrior = new Warrior();
            int[] expected = new int[] {5,2,1};
            //act
            int[] actual = new int[] { warrior.Strength, warrior.Dexterity, warrior.Intelligence };
            //assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void MageAttributes_NewMageCharacterAttributes_ShouldBeLevelOneAttributes()
        {
            // Arrange
            Mage mage = new Mage();
            int[] expected = new int[] {1, 1, 8};
            //act
            int[] actual = new int[] { mage.Strength, mage.Dexterity, mage.Intelligence };
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]

        public void RangerAttributes_NewRangerCharacterAttributes_ShouldBeLevelOneAttributes()
        {

            // Arrange
            Ranger ranger = new Ranger();
            int[] expected = new int[] { 1 ,7, 1 };
            //act
            int[] actual = new int[] { ranger.Strength, ranger.Dexterity, ranger.Intelligence };
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]

        public void RogueAttributes_NewRogueCharacterAttributes_ShouldBeLevelOneAttributes()
        {

            Rogue rogue = new Rogue();
            // Arrange
            int[] expected = new int[] { 2, 6 ,1 };
            //act
            int[] actual = new int[] { rogue.Strength, rogue.Dexterity, rogue.Intelligence };
            //Assert
            Assert.Equal(expected, actual);

        }


        //*******************************************************//
                // Level Two//
        //_*******************************************************//


        [Fact]
        public void WARAttributes_LevelTwoWARCharacterAttributes_ShouldBeLevelTwoAttributes()
        {
            // Arrange
            Warrior warrior = new Warrior();
            int[] expected = new int[] { 8, 4, 2 };
            //act
            warrior.LevelUP();
            int[] actual = new int[] { warrior.Strength, warrior.Dexterity, warrior.Intelligence };
            //Saaer
            Assert.Equal(expected, actual);


        }


        [Fact]
        public void MageAttributes_LevelTwoCharacterAttributes_ShouldBeLevelTwoAttributes()
        {
            // Arrange
            Mage mage = new Mage();
            int[] expected = new int[] { 2, 2, 13 };
            //act
            mage.LevelUP(); 
            int[] actual = new int[] { mage.Strength, mage.Dexterity, mage.Intelligence };
            //Assert
            Assert.Equal(expected, actual);

        }



        [Fact]

        public void RangerAttributes_RangerCharacterAttributes_ShouldBeLevelTwoAttributes()
        {
            Ranger ranger = new Ranger();
            // Arrange

            int[] expected = new int[] { 2, 12, 2};
            //act
            ranger.LevelUP();
            int[] actual = new int[] { ranger.Strength, ranger.Dexterity, ranger.Intelligence };
            //Assert
            Assert.Equal(expected, actual);
        }



        [Fact]

        public void RogueAttributes_RogueCharacterAttributes_ShouldBeLevelTwoAttributes()
        {

            Rogue rogue = new Rogue();
            // Arrange
            int[] expected = new int[] { 3, 10, 2 };
            //act
            rogue.LevelUP();    
            int[] actual = new int[] { rogue.Strength, rogue.Dexterity, rogue.Intelligence };
            //Assert
            Assert.Equal(expected, actual);

        }


        //*******************************************************//
        /* Items and equipment tests*/
        //*******************************************************//


  
        [Fact]

        public void EquipWapon_WeaponToHighLevel_SholdThrowError()
        {
            Warrior warrior = new Warrior();
            Weapon weapon = new Weapon()
            {
                ItemLevel = 2,
                WeaponType = WeaponType.Axes,
            };
            Assert.Throws<WeaponExeption>(() => warrior.EquipWapon( weapon));
        }


        [Fact]

        public void EquipArmor_ArmorToHighLevel_SholdThrowError()
        {
            Warrior warrior = new Warrior();
            Armor armor = new Armor()
            {
                ItemLevel = 2,
                ArmorType = ArmorType.Plate,
            };

            Assert.Throws<ArmorException>(() => warrior.EquipArmor( armor));
        }



        [Fact]

        public void EquipWapon_EquipWrongWoapentype_ShouldThrowError()
        {
            Warrior warrior = new Warrior();
            Weapon wapon = new Weapon()
            {
                WeaponType = WeaponType.Bows
               
            };

            // assert
            Assert.Throws<WeaponExeption>(() => warrior.EquipWapon( wapon));

        }


        [Fact]

        public void EquipArmor_EquipWrongArmortype_ShouldThrowError()
        {
           Warrior warrior = new Warrior();
           Armor armor = new Armor()
            {
                ArmorType = ArmorType.Cloth, 
                ItemSlot = SlotItem.Head

            };

            // assert

            Assert.Throws<ArmorException>(() => warrior.EquipArmor( armor));

        }

        [Fact]

        public void EquipWeapon_EquipWeopentype_ShouldEquipWeapon()
        {
            Warrior warrior = new Warrior();
            Weapon weapon = new Weapon()
            {
                WeaponType = WeaponType.Axes,
                ItemSlot = SlotItem.Weapon,
                ItemLevel = 1
            };
            string expected = "New weapon equipped";
            string actual = warrior.EquipWapon( weapon);

            Assert.Equal(expected, actual);

        }

        [Fact]

        public void EquipArmor_EquipArmortype_ShouldEquipArmor()
        {
            Warrior warrior = new Warrior();
            Armor armor = new Armor()
            {
                ArmorType = ArmorType.Plate,
                ItemSlot = SlotItem.Head,
                ItemLevel = 1
            };
            string expected = "Armor equip";
            string actual = warrior.EquipArmor( armor); 
    
            Assert.Equal(expected, actual); 

        }


        [Fact]

        public void CalculateDamage_CalculateWarriorWeoapon_ShouldReturnDamage()
        {
            //Arrange
            Warrior warrior = new Warrior();

            //Act
            warrior.Damage();
            double expected = 1.0 * (1.0 + (5.0 / 100.0));

            //Assert
            Assert.Equal(expected, warrior.CharacterDamage);
        }


        [Fact]
        public void CalculateDPS_CalculateDPSWarriorOfLevelOneAndEquippiedWeapon_ReturnDps()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapon weapon = new Weapon()
            {
                WeaponType = WeaponType.Axes,
                AttackSpeed = 1.1,
                Damage = 7,
                ItemLevel = 1,
                ItemSlot = SlotItem.Weapon,   
            };

            //Act
            warrior.EquipWapon( weapon);
            
            double expected = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));
            //Assert
            Assert.Equal(expected, warrior.DPS);

        }



        [Fact]
        public void CalcDamage_CalculateDamageOfWarriorWithWeaponAndArmor_ShouldReturnExpectedDamage()
        {
            //Arrange
            Warrior warrior = new Warrior();

            Weapon weapon = new Weapon()
            {
                
                WeaponType = WeaponType.Axes,
                AttackSpeed = 1.1,
                Damage =7,
                ItemLevel= 1,
                ItemSlot= SlotItem.Weapon,
          
            };
            Armor armor = new Armor()
            {

                ArmorType = ArmorType.Plate,
                ItemSlot = SlotItem.Body,
               Strength =  1,
            };

            //Act
            warrior.EquipArmor( armor);
            warrior.EquipWapon( weapon);
            double ExpectedDamage = (7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100.0 ));
            Assert.Equal(ExpectedDamage, warrior.DPS);

        }


    }


}
﻿using RPG_Characters.Eceptions;
using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Characters
{
    /// <summary>
    /// Abstract character class for the characters
    /// </summary>
    public abstract class Character
    {

        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public SlotItem[] AvaliableSlots { get; } = new SlotItem[] 
        { SlotItem.Weapon, SlotItem.Head, SlotItem.Body, SlotItem.Legs };
        public Dictionary<SlotItem, object> Equip { get; set; }
        public double CharacterDamage { get; set; }
        public double DPS { get; set; }
        public int PrimararyStatGain { get; set; }
        public int PrimararyStat { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int StrengthGain { get; set; }
        public int DexterityGain { get; set; }
        public int IntelligenceGain { get; set; }
        public WeaponType[] AllawedWeaponTypes { get; set; }
        public ArmorType[] AllawedArmorType { get; set; }



        /// <summary>
        /// Initialize Dictionary SlotItem and object
        /// </summary>

        public Character()
        {
            Equip = new Dictionary<SlotItem, object>();

        }

        /// <summary>
        ///  when a character gains a level it will be level 2
        ///  Assignment operator when the level is up 
        /// </summary>
        public void LevelUP()
        {
            Level++;
            Strength += StrengthGain;
            Dexterity += DexterityGain;
            Intelligence += IntelligenceGain;
            PrimararyStat += PrimararyStatGain;

        }

        /// <summary>
        /// Abstract EquipWeapon method for Weopen 
        /// Cheack if character equip a high level weapon or lower level weapon
        /// </summary>
        /// <param name="weapon">Weapon object</param>
        /// if the level is higher or lower then throw WeaponExeption message
        /// <returns> Message with a throws and exception</returns>
        /// <exception cref="WeaponExeption">When weapon level is higher or Lower or when
        /// weapon is not of type Staff or Wands</exception>

        public abstract string EquipWapon(Weapon weapon);


        /// <summary>
        /// inheritance  of EquipArmor
        /// </summary>
        /// <param name="armor">Weapon object</param>
        /// <returns>Message with a throws and exception</returns>
        /// <exception cref="ArmorException">
        /// Thrown  ArmorException when armor level is not valid or when armorType is not in armType
        /// </exception>

        public abstract string EquipArmor(Armor armor);


        /// <summary>
        /// Abstract method that calculate armor stats  
        /// </summary>
        /// <param name="armor">Armor object</param>
        public abstract void calculateStats(Armor armor);

   
        /// <summary>
        /// calculate a character damage
        /// 
        /// </summary>

        public void Damage()
        {

            if (Equip.ContainsKey(SlotItem.Weapon))
            {
                Weapon weapon = (Weapon)Equip[SlotItem.Weapon];
                CharacterDamage = 1.0 + (PrimararyStat / 100.0);
                DPS = weapon.DPS * CharacterDamage;
            }
            else
            {
                CharacterDamage = 1.0 * (1.0 + (PrimararyStat / 100.0));
                DPS = 1.0 * CharacterDamage;
                DPS = Math.Round(DPS, 4);

            }


        }
      
    }

}

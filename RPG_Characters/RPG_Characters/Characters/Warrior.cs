﻿using RPG_Characters.Eceptions;
using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Characters
{
    public class Warrior : Character
    {

        /// <summary>
        //Initialize a Warrior.
        /// Warrior are allowed to equip weapontype, Hammers, Swords and Axes,
        /// ArmorType Warrior are Maile and plate 
        /// </summary>

        public Warrior()
        {
            // Warrior begn level with 
            Strength = 5;
            Dexterity = 2;
            Intelligence = 1;
            //Warrior level up and thear gain 
            StrengthGain = 3;
            DexterityGain = 2;
            IntelligenceGain = 1;

            // Warrior base primary attributes 
            PrimararyStat = Strength;
            PrimararyStatGain = StrengthGain;

            AllawedWeaponTypes = new WeaponType[]
            { WeaponType.Hammers, WeaponType.Swords, WeaponType.Axes};

            AllawedArmorType = new ArmorType[]
            { ArmorType.Mail, ArmorType.Plate};


        }

        public override void calculateStats(Armor armor)
        {
            if (AvaliableSlots.Contains(armor.ItemSlot))
            {

                Strength += armor.Strength;
                Intelligence += armor.Intelligence;
                Dexterity += armor.Dexterity;

                PrimararyStat = Strength;
                Damage();

            }


        }


        public override string EquipArmor(Armor armor)
        {
            if (Level < armor.ItemLevel)
            {
                throw new ArmorException("The level is low ");

            }
            else
            {
                if (AvaliableSlots.Contains(armor.ItemSlot))
                {
                    //  Cheackes if armor armortype is with in armortype 
                    if (AllawedArmorType.Contains<ArmorType>(armor.ArmorType))
                    {
                        Equip[SlotItem.Legs] = armor;
                        calculateStats(armor);
                        return "Armor equip";
                    }
                    else
                    {
                        throw new ArmorException(" Not allowd to Equip armortype");
                    }
                }
                return " Some thing want wrong ";
            }
        }

        public override string EquipWapon(Weapon weapon)
        {
            if (Level < weapon.ItemLevel
          && weapon.WeaponType != WeaponType.Staffs
          || Level > weapon.ItemLevel && weapon.WeaponType != WeaponType.Wands)
            {
                throw new WeaponExeption("Level is Not Valid");

            }
            else if (AllawedWeaponTypes.Contains<WeaponType>(weapon.WeaponType))
            {
                Equip[SlotItem.Weapon] = weapon;
                Damage();
                return "New weapon equipped";
            }
            else
            {
                throw new WeaponExeption(" Wrong weaponType ");
            }
        }
    }
}

﻿using RPG_Characters.Eceptions;
using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Characters
{
    public class Mage : Character
    {
        public int bonusStrength;
        public int bonusIntelligence;
        public int bonusDexterity;

        public Mage() 
        {

            Strength = 1;
            Dexterity = 1;
            Intelligence = 8;

            StrengthGain = 1;
            DexterityGain = 1;
            IntelligenceGain = 5;

            PrimararyStat = Intelligence;
            PrimararyStatGain = IntelligenceGain;


            // un array that define wich WeaponType typ or ArmorType 
            AllawedWeaponTypes = new WeaponType[] { WeaponType.Staffs, WeaponType.Wands};
            AllawedArmorType = new ArmorType[] { ArmorType.Cloth };




        }

        public override void calculateStats( Armor armor)
        {
            if (Equip.ContainsKey(armor.ItemSlot))
            {
                Strength += armor.Strength;
                Intelligence += armor.Intelligence;
                Dexterity += armor.Dexterity;

                PrimararyStat = bonusIntelligence;
                Damage();

            }
        }

        public override string EquipArmor( Armor armor)
        {
            if (Level < armor.ItemLevel)
            {
                throw new ArmorException("The level is low ");

            }
            else
            {
                if (AvaliableSlots.Contains(armor.ItemSlot))
                {
                    if (AllawedArmorType.Contains<ArmorType>(armor.ArmorType))
                    {
                        Equip[SlotItem.Legs] = armor;
                        calculateStats(armor);
                        return "Armor equip";

                    }
                    else
                    {
                        throw new ArmorException(" Not allowd to Equip armortype");
                    }
                }
                return " Some thing want wrong ";
            }
        }

        public override string EquipWapon(Weapon weapon)
        {
            if (Level < weapon.ItemLevel
             && weapon.WeaponType != WeaponType.Staffs
             || Level > weapon.ItemLevel && weapon.WeaponType != WeaponType.Wands)
            {
                throw new WeaponExeption("Level is Not Valid");

            }

            else if (AllawedWeaponTypes.Contains<WeaponType>(weapon.WeaponType))
            {
                Equip[SlotItem.Weapon] = weapon;
                Damage();
                return "New weapon equipped";
            }
            else
            {
                throw new WeaponExeption(" Wrong weaponType ");
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    public class Armor :Item
    {
        

        public ArmorType ArmorType { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        
    }
    public enum ArmorType
    {
        // Armor typs 
        Cloth,
        Leather,
        Mail,
        Plate

    }


}

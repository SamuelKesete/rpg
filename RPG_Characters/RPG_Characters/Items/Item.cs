﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    public abstract class Item
    {
        // get and set proporits methods to specify item 
        public string ItemName { get; set; }    
        public int ItemLevel { get; set; }
        public SlotItem ItemSlot { get; set; }
    }
    


    public enum SlotItem
    {
        // item typs 
        Head,
        Body,
        Legs,
        Weapon

    }

 
}

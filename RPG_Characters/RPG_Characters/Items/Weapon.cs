﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
   

    public enum WeaponType
    {
        // Weapen typs 
        Axes,
        Bows,
        Daggers,
        Hammers,
        Staffs,
        Swords,
        Wands,

    }

    public class Weapon : Item
    {
      

        public WeaponType WeaponType { get; set; }
        public double AttackSpeed { get; set; }
        public int Damage { get; set; }
        public double DPS { get => AttackSpeed * Damage; }


        //public int Level { get; set; }

    }

    

    // ckeak
    public class WeaponAttributes
    {
        public int Damage { get; set; }
        public double Attack { get; set; }

    }

    


}
